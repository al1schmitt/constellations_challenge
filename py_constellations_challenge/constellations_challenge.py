"""
generate constellations queries
"""

import sys
import os
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import argparse
import random
from matplotlib import gridspec
from matplotlib.offsetbox import OffsetImage,AnnotationBbox

import PIL

from gtts import gTTS

from logger import logger

class ConstellationsQuery:
    """
    Query const

    Attributes:
    """

    def __init__(self, in_dirpath, opt_panel_type, opt_disable_speech):
        """
        """
        self.processing_quit = False
        self.disableEventHandling = False
        self.img_dirpath = in_dirpath
        self.enable_speech = not opt_disable_speech
        self.opt_panel_type = opt_panel_type
        self.img_fpath, self.queries_fpath  = self.load_data(opt_panel_type=opt_panel_type)
        
        self.load_queries(in_fpath=self.queries_fpath)

        self.n = len(self.queries)
        self.iteration = -1
        self.idx_already_played = []

        self.enable_evaluate_score = False
        self.score = 0
        self.success = 0

    def load_queries(self, in_fpath):
        """
        """
        self.queries = []
        with open(in_fpath, 'r') as f:
            for line in f:
                if "#" not in line and len(line)>1:
                    split_line = line.split("\t")
                    qu_d = {}
                    qu_d["query"] = split_line[0]
                    qu_d["h"] =  float(split_line[1])
                    qu_d["v"] = float(split_line[2].strip("\n"))
                    try:
                        qu_d["img_query"] = str(split_line[3].strip("\n"))
                    except:
                        qu_d["img_query"] = None
                    
                    logger.info("loading qu: {}".format(qu_d))
                    self.queries.append(qu_d)

    def query_all(self):
        """
        """
        self.n_queries = self.n

        logger.info("N queries: {}".format(self.n_queries))

        self.query_next()

        #deprecated
        if 0:
            for k in range(self.n_queries):
                if self.processing_quit:
                    exit()
                self.query_next()
            

    def show_query(self, title_override="", show_answer=False, enable_speech=True):
        mpl.rcParams['toolbar'] = 'None'

        if self.processing_quit:
            exit()

        self.fig = plt.figure( "Constellations challenge", figsize=(14,10), facecolor='black')
        self.fig.suptitle("{}".format(title_override), size=24, y=0.95, color="w", alpha=0.8)
        
        self.query_str = self.queries[self.iteration]["query"]
        self.answer = {"h":self.queries[self.iteration]["h"], "v":self.queries[self.iteration]["v"]}
        
        self.tol = 140
        if self.opt_panel_type == "deepsky":
            self.tol=20

        type_str = "la constellation"
        if self.opt_panel_type == "deepsky":
            type_str = "l'objet"

        self.canvas = self.fig.canvas
        self.axes = []

        remove_n_rows = 0

        rows=1
        columns=3

        gs = gridspec.GridSpec(rows, columns, width_ratios=[1, 0.1, 3])

        self.axes = []
        for ks in range(columns):
            ax = plt.subplot(gs[ks])
            self.axes.append(ax)

        big_fontsize = 16
        normal_fontsize = 14

        text_start_pos = -750

        #self.axes.append(self.fig.add_subplot(rows, columns, 1))
        #self.axes[0].plot([0, 0, 1000], [0, 1000, 1000], "k")
        #self.axes[0].plot([160, 160], [-10, 120], "w", alpha=0.1)
        self.axes[0].text(text_start_pos, 3000, "SCORE: {:.0f}".format(self.score), color="w", fontsize=normal_fontsize, alpha=0.8)
        
        self.axes[0].set_xlim(-400, 1000)
        self.axes[0].set_ylim(200, 1000)

        if show_answer:
            if self.success:
                _color = "g"
                self.axes[0].text(text_start_pos, 2400, "BRAVO !".format(), color=_color, fontsize=normal_fontsize, alpha=0.7)


                if enable_speech:
                    text_tts = "Gagné !".format()
                    language = "fr"
                    speech = gTTS(text=text_tts, lang=language, slow=True)
                    speech.save("tmp_tts.mp3")
                    os.system("play tmp_tts.mp3 &")
                
            else:
                _color = "r"
                self.axes[0].text(text_start_pos, 2400, "FAUX !".format(), color=_color, fontsize=normal_fontsize, alpha=0.7)

                if enable_speech:
                    text_tts = "Perdu !".format()
                    language = "fr"
                    speech = gTTS(text=text_tts, lang=language, slow=True)
                    speech.save("tmp_tts.mp3")
                    os.system("play tmp_tts.mp3 &")
                    
            self.axes[0].text(text_start_pos, 2000, "Réponse : \n".format(self.query_str), color=_color, fontsize=normal_fontsize, alpha=0.5)
        else:
            self.axes[0].text(text_start_pos, 2000, "Clique sur {}: \n".format(type_str), color="w", fontsize=normal_fontsize, alpha=0.5)        

            if self.enable_speech:
                text_tts = "Trouver {} {}".format(type_str, self.query_str)
                language = "fr"
                speech = gTTS(text=text_tts, lang=language, slow=True)
                speech.save("tmp_tts.mp3")
                os.system("play tmp_tts.mp3 &")
                
        if self.opt_panel_type == "deepsky":
            imageFile = os.path.join(self.img_dirpath, self.queries[self.iteration]["img_query"])
            logger.info("showing query image: {}".format(imageFile))
            img=mpimg.imread(imageFile)
        else:
            grey_level = 0
            #img = PIL.Image.new('RGBA', [250, 250], (grey_level,grey_level,grey_level))
            img = np.ones((250, 250, 3))*grey_level
        
        tx, ty = 0, 0
        size = 1600
        #extent =[0,.5,0,.5]
        self.axes[0].imshow(img, extent=(tx, tx + size, ty, ty + size))
        
        self.axes[0].text(text_start_pos, 1900, "{}".format(self.query_str), color="w", fontsize=big_fontsize)
        self.axes[0].text(text_start_pos, -900, "q: quit", color="w", fontsize=16, alpha=0.4)
        if show_answer:
            self.axes[0].text(text_start_pos, -1100, "left click: next question", color="w", fontsize=16, alpha=0.4)
        
        #self.axes[0].get_xaxis().set_visible(False)
        #self.axes[0].get_yaxis().set_visible(False)
        #self.axes[0].text(2000, 100, "XXXXXXXXXXXXXXXXX")
        self.axes[1].set_facecolor((.0, 0., 0.))
        self.axes[0].set_facecolor((.0, 0., 0.))
        self.axes[2].set_facecolor((.0, 0., 0.))

        #show image at the right
        img_i=mpimg.imread(self.img_fpath)
        #self.axes.append(self.fig.add_subplot(rows, columns, 2))
        self.axes[2].imshow(img_i[remove_n_rows:, :])

        if show_answer:
            #add answer location
            self.axes[2].plot([self.answer["h"]-self.tol, self.answer["h"]+self.tol], [self.answer["v"]+self.tol, self.answer["v"]+self.tol], _color)
            self.axes[2].plot([self.answer["h"]+self.tol, self.answer["h"]-self.tol], [self.answer["v"]-self.tol, self.answer["v"]-self.tol], _color)
            self.axes[2].plot([self.answer["h"]-self.tol, self.answer["h"]-self.tol], [self.answer["v"]-self.tol, self.answer["v"]+self.tol], _color)
            self.axes[2].plot([self.answer["h"]+self.tol, self.answer["h"]+self.tol], [self.answer["v"]+self.tol, self.answer["v"]-self.tol], _color)
        self.axes[2].get_xaxis().set_visible(False)
        self.axes[2].get_yaxis().set_visible(False)
        #self.axes[2].set_title("Please click on...")
        

        self.axes[0].set_xlim(-400,1500)
        self.axes[0].set_ylim(200,1500)

        if not show_answer:
            self.enable_evaluate_score = True
        else:
            self.enable_evaluate_score = False

        #plt.tight_layout()
        plt.subplots_adjust(top=0.9) 
        plt.pause(1)
        if not self.disableEventHandling:
            self.startEventHandling()
        else:
            self.fig.show()
            plt.show()


    def startEventHandling(self):
        print("start event handling: ...")
        print("\t Click on the map to select a constellation")
        print("\t")
        self.coords = []
        # Call click func
        cid1 = self.fig.canvas.mpl_connect('button_press_event', self.onclick)
        cid2 = self.fig.canvas.mpl_connect('key_press_event', self.on_key_press)
        cid3 = self.fig.canvas.mpl_connect('draw_event', self.ondraw)

        self.enablepointpicking = True
        
        plt.show()
        self.fig.canvas.mpl_disconnect(cid2)
        
    ##callback for point picking event    
    def onclick(self, event):
        global ix, iy
        if event.button == 1: #1=left click, 3=rightclick
            if True:   
                if self.enable_evaluate_score:   
                    ix, iy = event.xdata, event.ydata
                    print("x = {:.2f}, y = {:.4e}".format(float(ix), float(iy)))
                    print("ans_h = {:.2f}, ans_v = {:.4e}".format(float(self.answer["h"]), float(self.answer["v"])))  

                    #circle
                    #success_condition = np.sqrt((self.answer["h"]-ix)**2+(self.answer["v"]-iy)**2)<self.tol 
                    
                    #square
                    success_condition = np.abs(self.answer["h"]-ix)<self.tol and np.abs(self.answer["v"]-iy)<self.tol 
                    if success_condition:
                        self.score += 1
                        self.success = 1
                        logger.info("Answer found !! BRAVO, new score : {}".format(self.score))
                    else:
                        self.score += 0
                        self.success = 0

                #plt.close()
                plt.clf()
                self.coords.append((ix, iy))
                self.refresh()

                if not self.enable_evaluate_score:
                    self.query_next()
                else:
                    title_str = ""
                    self.show_query(title_override=title_str, show_answer=True, enable_speech=self.enable_speech)

        if self.enablepointpicking:
            print("INFO: there are {} points in store".format(len(self.coords)))

    def ondraw(self, event):
        """
        """
        pass

    def on_key_press(self, event):
        print("key pressed = {}".format(event.key))
        if event.key == 'q':
            self.processing_quit = True
            exit()

    def refresh(self):
        self.canvas.draw()

    def query_next(self):
        """
        """

        if self.processing_quit:
            exit()

        if len(self.idx_already_played)>=len(self.queries):
            exit()

        if 0:
            self.iteration += 1
        else:
            self.iteration = -1
            while (self.iteration==-1) or self.iteration in self.idx_already_played:
                self.iteration = int(random.random()*self.n)
                logger.info("Randomly selected iteration={}".format(self.iteration))

        logger.info("Iteration finally selected = {}".format(self.iteration))

        self.idx_already_played.append(self.iteration)
        title_str = ""
        self.show_query(title_override=title_str, enable_speech=self.enable_speech)
        #self.show_query(title_override=title_str, show_answer=True, enable_speech=self.enable_speech)


        
    def load_data(self, opt_panel_type):
        """
        """

        if opt_panel_type=="cubes":
            img_fpath = os.path.join(self.img_dirpath, "img_cubes_1_9x9.jpg")
            queries_fpath = os.path.join(self.img_dirpath, "qatable_cubes_1.txt")
        elif opt_panel_type=="deepsky":
            img_fpath = os.path.join(self.img_dirpath, "img_deepsky_1_9x9.png")
            queries_fpath = os.path.join(self.img_dirpath, "qatable_deepsky_1.txt")
        else:
            n_stars_qa = 2
            k_qa = int(np.random.random()*n_stars_qa)+1
            img_fpath = os.path.join(self.img_dirpath, "img_stars_{}_9x9.png".format(k_qa))
            queries_fpath = os.path.join(self.img_dirpath, "qatable_stars_{}.txt".format(k_qa))
            
        return img_fpath, queries_fpath 


def listFiles(f1Directory, inclusion_string=None):
    """
    Scans input directories and build files list whose shape is Nx1
    """
    fileList = []
	
    for i,file in enumerate(sorted(os.listdir(f1Directory))):
        if inclusion_string is None:
            incl_valid = True
        else:
            logger.debug("Inclusion string is {}".format(inclusion_string))
            logger.debug("str(file) string is {}".format(str(file)))
            incl_valid = (inclusion_string in str(file))
        if (file.endswith(".png")) and incl_valid: 
            a1 = str(file)
            fileList.append(a1)
    return fileList


if __name__ == "__main__":
    """
    """
    logger.info("Starting Constellations challenge now.")

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, description="""
                            Processing Constellations challenge:
                            """)

    parser.add_argument('-i', 
                        action="store", 
                        dest="dir_path", 
                        default="./data", 
                        help="path to the input queries directory")

    parser.add_argument('-k', 
                        action="store", 
                        dest="panel_type", 
                        default="cubes", 
                        help="which panel to use. choose between: cubes, stars, deepsky")

    parser.add_argument('-s', 
                        action="store_true", 
                        dest="opt_disable_speech",
                        help="Disable text to speech")

    args = parser.parse_args()

    if os.path.isdir(args.dir_path):
        fullinputdirpath = os.path.abspath(args.dir_path)
    else:
        logger.error("input directory does not exist. aborting")
        exit()

        
    mcq = ConstellationsQuery(in_dirpath=fullinputdirpath, opt_panel_type=args.panel_type, opt_disable_speech=args.opt_disable_speech) 
    mcq.query_all()
